import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../providers/auth.service';
import { snapshotChanges } from '@angular/fire/database';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css'],
  providers:[AuthService]
})
export class TodoComponent implements OnInit {

  toDoListArray: any[];
  constructor(private auth: AuthService,) { }

  ngOnInit() {
    this.auth.getToDoList().snapshotChanges()
    .subscribe(item => {
      this.toDoListArray = [];
      item.forEach(element => {
        var x = element.payload.toJSON();
        x["$key"] = element.key;
        this.toDoListArray.push(x);
      })

      // code to sort the list if it is checked or not
      this.toDoListArray.sort((a,b) =>{
        return a.isChecked - b.isChecked;
      })
    });
  }

    // function that will be added to the input
    onAdd(itemTitle){
      this.auth.addTitle(itemTitle.value);
      itemTitle.value = null
    }
    alterCheck($key: string,isChecked){
      this.auth.checkOrUnCheckTitle($key,!isChecked);
    }
}
