import { Component, OnInit } from '@angular/core';
import { FirebasedbService } from 'src/app/providers/firebasedb.service';


@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {

  constructor(
    private firedb: FirebasedbService
  ) { }

  ngOnInit() {
    this.firedb.getFireData('users')
    .then((res: any) => {
      console.log(res);
    })
    .catch((res: any) => {
      console.log(res);
    });
  }

}
