import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FineloginComponent } from './finelogin.component';

describe('FineloginComponent', () => {
  let component: FineloginComponent;
  let fixture: ComponentFixture<FineloginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FineloginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FineloginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
