import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './pages/auth/login/login.component';
import { RegistrationComponent } from './pages/auth/registration/registration.component';
import { HomeComponent } from './pages/home/home.component';
import { FineloginComponent } from './pages/finelogin/finelogin.component';
import { LandingComponent } from './pages/landing/landing.component';
import { TodoComponent } from './pages/todo/todo.component';


const routes: Routes = [
  { path: "", pathMatch: "full", redirectTo: "landing"},
  { path: "landing", component: LandingComponent},
  { path: "login", component: LoginComponent},
  { path: "register", component: RegistrationComponent},
  { path: "home", component: HomeComponent},
  { path: "finelog", component: FineloginComponent}, 
  { path: "todo", component: TodoComponent}, 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
