import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore} from '@angular/fire/firestore';
import { Router} from '@angular/router';
import { BehaviorSubject } from 'rxjs';

// for todolist
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { Title } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private eventAuthError = new BehaviorSubject<string>("");
  eventAuthError$ = this.eventAuthError.asObservable();
  newUser: any;

  // todolist variable
  toDoList: AngularFireList <any>;

  constructor(private afAuth: AngularFireAuth, private db: AngularFirestore, private router: Router, private firebasedb: AngularFireDatabase) { }

  getUserState(){
    return this.afAuth.authState;
  }
  
  login(email:string, password:string){
    this.afAuth.auth.signInWithEmailAndPassword(email,password)
    .catch(error =>{
      this.eventAuthError.next(error);
    })
    .then(userCredential => {
      if(userCredential){
        this.router.navigate(['/home']);
      }
    })
  }

  createUser(user){
    this.afAuth.auth.createUserWithEmailAndPassword(user.email, user.password)
    .then( userCredential =>{
      this.newUser = user;
        console.log(userCredential);
      userCredential.user.updateProfile({
        displayName: user.firstName +''+user.lastname
      });

      this.insertUserData(userCredential)
      .then ( () =>{
        this.router.navigate(['./home']);
      });  
    }) 

    .catch (error => {
      this.eventAuthError.next(error);
    })
  }

  insertUserData(userCredential: firebase.auth.UserCredential){
    return this.db.doc('users/${userCredential.user.uid}').set({
      email: this.newUser.email,
      firstname: this.newUser.firstname,
      lastname: this.newUser.lastname,
      role: 'network user'
    })
    
  }

  logout(){
    return this.afAuth.auth.signOut();
  }

  // codes to save all of our data inside title in firebase
  getToDoList(){
    this.toDoList = this.firebasedb.list('titles');
    return this.toDoList;
  }

  // if we want to add item inside the firebase of todolist, HERE is the code to add item.
  addTitle(title: string){
     this.toDoList.push({
       title: title,
       isChecked: false
     });
  }
    // whenever we add an item inside our database, we need a unique ket that will identify that item which is '$key'
  // this code is useful in this place because we want to check and uncheck a button
  checkOrUnCheckTitle($key: string, flag: boolean){
    this.toDoList.update($key, { isChecked: flag});
  }

  // Here is the code to remove data from database
  removeTitle($key: string){
    this.toDoList.remove($key);
  }

}
