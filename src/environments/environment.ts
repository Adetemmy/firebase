// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAXponGRNZ36JHDXfSU0M32KqjLqV7vOrc",
    authDomain: "webapp-2f63f.firebaseapp.com",
    databaseURL: "https://webapp-2f63f.firebaseio.com",
    projectId: "webapp-2f63f",
    storageBucket: "webapp-2f63f.appspot.com",
    messagingSenderId: "363875622147",
    appId: "1:363875622147:web:e5f03aa32acfa7b62d1d36",
    measurementId: "G-3E0NSYD3MG"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
